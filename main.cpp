#include <stdio.h>
#include<stdlib.h>
#include<string.h>

using namespace std;

int tmp_size=50;

typedef struct llist
{
    char* name;
    int number[20];
    int number_size;
}List;

typedef struct Found
{
    int Found_id;
}Found;

List *Llist=(List*)malloc(sizeof(List)*tmp_size);

/*------------------------------------------------------*/
char * readNumber(char *ptr, int id, List *Llist, int *check);
int addData(char *ptr, int id, int *check);
int findData(char *ptr, int id);
int isNumber(char number);
int isAlfa(char alfa);
char * readName(char *ptr, int id, List *Llist, int *check);
void freeMem(List *Llist, int L_count);
int TOT9(char alfa);
/*------------------------------------------------------*/
void freeMem(List *Llist, int L_count)
{
    int i;
    for (i=0; i<L_count; i++)
    {
        free(Llist[i].name);
    }
    free(Llist);
}
/*------------------------------------------------------*/
int addData(char *ptr, int id, int *check)
{
    int numCheck=0, nCheck=0;

    //printf("pridavam\n");
    ptr++;
    if(*ptr!=' ')
        return 0;
    ptr++;
    //printf("predavane id: %d", id);
    ptr=readNumber(ptr, id, Llist, &numCheck);
    if(!ptr)
        return 0;
    if(*ptr!=' ')
        return 0;
    ptr++;
    ptr=readName(ptr, id, Llist, &nCheck);
    if(!ptr)
	{free(Llist[id].name);
	return 0;}

    if(numCheck && nCheck)
        {*check=1;
	free(Llist[id].name);}
        else *check=0;

    return 1;
}
/*------------------------------------------------------*/
char * readNumber(char *ptr, int id, List *Llist, int *check)
{
    int num, i,j, Ncount=0;
    int number[20];

    *check=0;

    while(*ptr!=' ')
    {
        if(isNumber(*ptr))
            {
		if(Ncount>=20)
                    return NULL;
                //printf("je to cislo: %c - ", *ptr);
                num=(int)(*ptr)-48;
                //printf("prevedene na int: %d\n", num);
                number[Ncount]=num;
                //printf("pridame na pozici %d -> %d\n", Ncount, number[Ncount]);
                Ncount++;
            }
	else return NULL;
        ptr++;
    }

    if(Ncount==0)
	return NULL;

    Llist[id].number_size=Ncount; //o jedno vetsi

    for (i=0;i<Ncount;i++)
    {
        Llist[id].number[i]=number[i];
    }

    for(i=0; i<id; i++)
    {
        if(Llist[i].number_size==Ncount)
        {
            for (j=0; j<Ncount; j++)
            {
                if(Llist[i].number[j]==number[j])
                    {*check=1;
			//printf("Shodne: %d %d\n", Llist[i].number[j], number[j]);
			}
                else {*check=0;break;}
            }
        }
        if((*check)==1)
           { //printf("shodne id: %d\n", i);
		break;
		}
    }

    return ptr;
}
/*------------------------------------------------------*/
int isNumber(char number)
{
 if(number<'0' || number>'9')
    return 0;
 else return 1;
}
/*------------------------------------------------------*/
int isAlfa(char alfa)
{
 if((alfa>='A' && alfa<='Z')|| (alfa>='a'&& alfa<='z'))
    return 1;
 else return 0;
}
/*------------------------------------------------------*/
char * readName(char *ptr, int id, List *Llist, int *check)
{
    int Asize=50,i;
    int Acount=0;

    Llist[id].name=(char*)malloc(Asize+1);

    *check=0;

    while(*ptr!='\0' && *ptr!='\n')
    {
        //printf("ptr: %c| Acount: %d|\n", *ptr, Acount);
 	//printf("isAlfa: %d\n", isAlfa(*ptr));
        if(isAlfa(*ptr))
            {
                //printf("je to pismeno: %c - ", *ptr);
                Llist[id].name[Acount]=(*ptr);
                //printf("pridame na pozici %d -> %c ...", Acount, Llist[id].name[Acount]);
                Acount++;
                if(Acount>=Asize)
                    Llist[id].name=(char*)realloc(Llist[id].name,(Asize*=2)+1);
            }
	else if (*ptr==' ' && Acount==0)
		return NULL;
	else if (*ptr==' ' && Acount!=0)
        {
            //printf("je to mezera - ");
            Llist[id].name[Acount]=(*ptr);
            //printf("pridame na pozici %d -> %c ...", Acount, Llist[id].name[Acount]);
            Acount++;
            if(Acount>=Asize)
                Llist[id].name=(char*)realloc(Llist[id].name,(Asize*=2)+1);
        }
        else return NULL;

        Llist[id].name[Acount]='\0';
        ptr++;
        //printf("ptr ukazuje:%c|\n", *ptr);
    }

    if(Acount==0)
	return NULL;

    if (Llist[id].name[Acount-1]==' ')
	return NULL;

    //printf("kontrola name check.\n");
    for(i=0; i<id; i++)
    {
        if(strlen(Llist[i].name)==strlen(Llist[id].name))
        {
            //printf("delky %d %d jsou stejne.\n", i, id);
            //for (j=0; j<Acount; j++)
            //{
		if(!strcmp(Llist[i].name,Llist[id].name))
                    *check=1;
                else *check=0;
            //}
            //printf("check: %d\n", *check);
        }
        if((*check)==1)

        {//printf("name check: %d\n", *check);
            break;}
    }

    //printf("name check: %d\n", *check);

    return ptr;

}
/*------------------------------------------------------*/
int findData(char *ptr, int id)
{
    Found *FOUND;
    int ID=0,F_size=50, i,j, num, Ncount=0, n_size=50, tmpid=0;
    int *number;
    char check=0, check_found=0;

    FOUND=(Found*)malloc(sizeof(Found)*F_size);
    number=(int*)malloc(sizeof(*number)*n_size);

    //printf("Hledam\n");
    //printf("ptr: %c\n", *ptr);
    if(*ptr!=' ')
	{free(number);
	free(FOUND);
        return 0;}
    ptr++;
    while(*ptr!='\0' && *ptr!='\n')
    {
        //printf("ptr: %c\n", *ptr);
        if(isNumber(*ptr))
            {
                num=(int)(*ptr)-48;
                number[Ncount]=num;
		//printf("nactene cislo: %d", number[Ncount]);
                Ncount++;
                if(Ncount>=n_size)
                    number=(int*)realloc(number, sizeof(int)*(n_size*=2));
            }
        else
            {
		free(FOUND);
		free(number);
                return 0;
            }
        ptr++;
    }

     if (Ncount==0)
	{free(FOUND);
	free(number);
        return 0;}
    //---------prohledani cisel-----------------------------------------
    if(Ncount<=20)
    {
        for(i=0;i<id;i++)
            {
		if(Ncount<=Llist[i].number_size)
                {
		for(j=0;j<Ncount;j++)
                    {
			//printf("%d. prohledavane: %d - %d nactene\n",i,Llist[i].number[j], number[j]);
                        if(Llist[i].number[j]==number[j])
                            check=1;
                        else {check=0;
				break;}
                    }
                if(check)
                {
                    FOUND[ID].Found_id=i;
                    ID++;
                    if(ID>=F_size)
                        FOUND=(Found*)realloc(FOUND,sizeof(Found)*(F_size*=2));
                }
		}
            }
        }
    //---------prohledani jmen-----------------------------------------
      for(i=0;i<id;i++)
            {
                if(Ncount<=(int)strlen(Llist[i].name))
                {
                    for(j=0;j<Ncount;j++)
                    {
                        if((TOT9(Llist[i].name[j]))==number[j])
                            check=1;
                        else {check=0;
				break;}
                    }
                    for (j=0;j<ID;j++)
                        {
                            if(FOUND[j].Found_id==i)
                                {check_found=1;
                                break;}
                            else check_found=0;

                        }
                    if(check && !check_found)
                        {
                        FOUND[ID].Found_id=i;
                        ID++;
                        if(ID>=F_size)
                        FOUND=(Found*)realloc(FOUND,sizeof(Found)*(F_size*=2));
                        }
                }
            }
    if(ID>10)
        printf("Celkem: %d\n", ID);
    else
        {
            for(i=0;i<ID;i++)
                {
                    tmpid=FOUND[i].Found_id;
                    for (j=0;j<Llist[tmpid].number_size;j++)
                    {
                        printf("%d", Llist[tmpid].number[j]);
                    }
                    printf(" ");
                    for(j=0;j<(int)strlen(Llist[tmpid].name);j++)
                    {
                        printf("%c", Llist[tmpid].name[j]);
                    }
                    printf("\n");
                }
                printf("Celkem: %d\n", ID);
        }
    free(FOUND);
    free(number);
    return 1;

}
/*------------------------------------------------------*/
int TOT9(char alfa)
{
    if(alfa=='a'||alfa=='A'||alfa=='b'|| alfa=='B'|| alfa=='c'|| alfa=='C')
        return 2;
    else if (alfa=='d'||alfa=='D'||alfa=='e'|| alfa=='E'|| alfa=='f'|| alfa=='F')
        return 3;
    else if (alfa=='g'||alfa=='G'||alfa=='h'|| alfa=='H'|| alfa=='i'|| alfa=='I')
        return 4;
    else if (alfa=='j'||alfa=='J'||alfa=='k'|| alfa=='K'|| alfa=='l'|| alfa=='L')
        return 5;
    else if (alfa=='m'||alfa=='M'||alfa=='n'|| alfa=='N'|| alfa=='o'|| alfa=='O')
        return 6;
    else if (alfa=='p'||alfa=='P'||alfa=='q'|| alfa=='Q'|| alfa=='r'|| alfa=='R' || alfa=='s'|| alfa=='S')
        return 7;
    else if (alfa=='t'||alfa=='T'||alfa=='u'|| alfa=='U'|| alfa=='v'|| alfa=='V')
        return 8;
    else if (alfa=='w'||alfa=='W'||alfa=='x'|| alfa=='X'|| alfa=='y'|| alfa=='Y' || alfa=='z'|| alfa=='Z')
        return 9;
    else if (alfa==' ')
        return 1;
    else return 0;
}
/*------------------------------------------------------*/

int main()
{
    //char c;
    char* input=NULL;
    char* ptr;
    int id=0;
    int check=0;
    size_t len = 0;
    int read=0;

    //int input_size=50;

	//input=(char*)malloc(sizeof(*input)*input_size);

    while ((read = getline(&input, &len, stdin))!=-1)
    {
  	// printf("nacteno %d znaku %lu bytu\n", read, len);
        ptr=input;

        if(*ptr=='+')
        {
            if(!addData(ptr, id, &check))
                {
                 printf("Nespravny vstup.\n");

                }
	    else if(!check)
                {
                    printf("OK\n");
                    id++;
                    if(id==tmp_size)
                        Llist=(List*)realloc(Llist, sizeof(List)*(tmp_size*=2));
                }
            else printf("Kontakt jiz existuje.\n");

            ptr++;
        }
        else if(*ptr=='?')
        {
            ptr++;
            if(!findData(ptr, id))
                {
                 printf("Nespravny vstup.\n");
                }
        }
        else
            {
                printf("Nespravny vstup.\n");
            }
    }
    free(input);
    freeMem(Llist, id);

    return 0;
}
